from MatrixLibrary import *
from MatrixTestGen import *
from random import randint

"""
 Start to generate matrix and to apply functions form the MatrixLibrary to them.
 The matrix and the results are printed out in a file with given name.
 file_name The name of the output file
"""
def matrix_start_test(file_name):
    f = open(file_name, 'w')
    number_tests = 10
    maxim_dimension = 300

    f.write('Sum test \n\n')
    for i in range(number_tests):
        
        dimension = randint(1, maxim_dimension)
        matrix1 = matrix_generate(dimension)
        matrix2 = matrix_generate(dimension)
        
        print_matrix('Matrix1 :', matrix1, f)
        print_matrix('Matrix2 :', matrix2, f)
        print_matrix('Sum of matrix1 and matrix2 :', add_matrix(matrix1, matrix2), f)

    f.write('Multiplication test \n\n')
    for i in range(number_tests):
        dimension = randint(1, maxim_dimension)
        matrix1 = matrix_generate(dimension)
        matrix2 = matrix_generate(dimension)
        print_matrix('Matrix1 :', matrix1, f)
        print_matrix('Matrix2 :', matrix2, f)
        print_matrix('Matrix1 multiplied by matrix2 :', multiply_2_matrices(matrix1, matrix2), f)

    f.write('Multiplication test: \n\n')
    for i in range(number_tests):
        dimension = randint(1, maxim_dimension)
        multiplier = randint(0, 100)
        matrix = matrix_generate(dimension)
        f.write('The multiplier = ' + str(multiplier) +'\n')
        print_matrix('The matrix:', matrix, f)
        print_matrix('The matrix multiplied', multiply(matrix, multiplier), f)

    f.write('Power test: \n\n')
    for i in range(number_tests):
        dimension = randint(1, maxim_dimension)
        exponent = randint (0, 5)
        matrix = matrix_generate(dimension)
        f.write('Exponent = ' + str(exponent) + '\n')
        print_matrix('The matrix :', matrix, f)
        print_matrix('The matrix rised to the exponent:', matrix_power(matrix, exponent), f)

    f.write('Get secondary diagonal: \n\n')
    for i in range(number_tests):
        dimension = randint(1, maxim_dimension)
        matrix = matrix_generate(dimension)
        print_matrix('The matrix', matrix, f)
        f.write('The scondary diagonal :\n')
        print_array(get_secondary_diagonal(matrix), f)

    f.write('Mirrored test: \n\n')
    for i in range(number_tests):
        dimension = randint(1, maxim_dimension)
        matrix = matrix_generate_mirrored(dimension)
        print_matrix('The matrix :', matrix, f)
        f.write('The matrix is mirrored') if is_mirrored(matrix) else f.write('The matrix is not mirrored')
        matrix = matrix_generate(dimension)
        print_matrix('The matrix :', matrix, f)
        f.write('The matrix is mirrored') if is_mirrored(matrix) else f.write('The matrix is not mirrored')           

    f.write('Triangular test: \n\n')
    for i in range(number_tests):
        dimension = randint(1, maxim_dimension)
        matrix = matrix_generate_triangular(dimension)
        print_matrix('The matrix :', matrix, f)
        f.write('The matrix is triangular') if is_upper_triangular(matrix) or is_lower_triangular(matrix) else f.write('The matrix is not triangular')
        matrix = matrix_generate(dimension)
        print_matrix('The matrix :', matrix, f)
        f.write('The matrix is triangular') if is_upper_triangular(matrix) or is_lower_triangular(matrix) else f.write('The matrix is not triangular')

