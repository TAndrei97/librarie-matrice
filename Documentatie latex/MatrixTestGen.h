/// \file MatrixTestGen.h
/// \brief C library implementation to generate matrices.
///

#ifndef MATRIXTESTGEN_H_INCLUDED
#define MATRIXTESTGEN_H_INCLUDED
#include "MatrixLibrary.h"

Matrix* matrix_generate(int max_dimension);
Matrix* matrix_generate_triangular(int max_dimension);
Matrix* matrix_generate_mirrored(int max_dimension);

#endif // MATRIXTESTGEN_H_INCLUDED
