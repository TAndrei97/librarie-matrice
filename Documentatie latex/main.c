/// \file main.c
/// \brief C implementation and testing of some matrix operations.
///

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "MatrixAutoRun.h"


int main()
{
    char a[30] = "output.txt";
    matrix_start_test(a);

    return 0;
}
