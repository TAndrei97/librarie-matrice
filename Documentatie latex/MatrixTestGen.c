/// \file MatrixTestGen.c
/// \brief C library implementation to generate matrices.
///

#include <stdio.h>
#include <stdlib.h>
#include "MatrixTestGen.h"
#include "MatrixLibrary.h"

    /// \fn Matrix* matrix_generate(int dimension)
    /// \brief Generate a random matrix
    ///
    /// \param dimension The dimension of the matrix
    /// \return A random matrix with dimension x dimension elements.
Matrix* matrix_generate(int dimension) {

    ///
    /// Create a structure of type matrix with dimension x dimension random elements.
    /// Return a pointer to that structure.
    ///

    int i;
    float* values = (float*)malloc(dimension * dimension * sizeof(float));


    for(i = 0; i < dimension*dimension; i++) {
        values[i] = rand()%10;
    }

    return init(dimension, dimension, values);


}

    /// \fn matrix_generate_triangular(int dimension)
    /// \brief Generate a random triangular matrix
    ///
    /// \param dimension The dimension of the matrix
    /// \return A random triangular matrix with dimension x dimension elements.
Matrix* matrix_generate_triangular(int dimension) {

    ///
    /// Create a structure of type matrix with dimension x dimension random elements.
    /// All the elements above or below the main diagonal are 0.
    /// Return a pointer to that structure.
    ///

    int i;
    int j;
    int triangular = rand()%2;
    float* values = (float*)calloc(dimension * dimension, sizeof(float));

    if(triangular == 0) {
        for(i = 0 ; i < dimension; i++) {
            for(j = i; j < dimension; j++) {
                values[i * dimension + j] = rand() % 10;
            }
        }
    }
    else {
        for(i = 0; i < dimension; i++) {
            for(j = 0; j <= i; j++) {
                values[i * dimension + j] = rand()%10;
            }
        }
    }
    return init(dimension, dimension, values);
}

    /// \fn matrix_generate_mirrored(int dimension)
    /// \brief Generate a random mirrored matrix
    ///
    /// \param dimension The dimension of the matrix
    /// \return A random mirrored matrix with dimension x dimension elements.
Matrix* matrix_generate_mirrored(int dimension) {

    ///
    /// Create a structure of type matrix with dimension x dimension random elements.
    /// All the elements against the main or second diagonal are equal.
    /// Return a pointer to that structure.
    ///

    int i;
    int j;
    int mirrored = rand()%2;
    float* values = (float*)malloc(dimension * dimension * sizeof(float));

    if(mirrored == 0) {
        for(i = 1; i < dimension; i++) {
            for(j = 0; j < i; j++) {
                values[i * dimension + j] = values[j * dimension + i] = rand()%10;
            }
            values[i * dimension + i] = rand()%10;
        }
    }
    else {
        for(i = 0; i < dimension; i++) {
            for(j = 0; j < dimension - 1 - i; j++) {
                values[i * dimension + j] = values[(dimension - j - 1) * dimension + (dimension- i - 1)] = rand()%10;
            }
            values[i * dimension + dimension - 1 - i] = rand()%10;
        }
    }

    return init(dimension, dimension, values);
}

