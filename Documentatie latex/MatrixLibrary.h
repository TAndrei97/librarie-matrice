/// \file MatrixLibrary.c
/// \brief C library implementation for matrix functions.
///

#ifndef MATRIXLIBRARY_H_INCLUDED
#define MATRIXLIBRARY_H_INCLUDED
#include <stdbool.h>

typedef struct Matrix Matrix;

void print_matrix(Matrix* mat, FILE* file, char* mess);
void print_array(float* a, int length, FILE* file);
void delete_matrix(Matrix* a);

bool are_summable(Matrix* a, Matrix* b);
bool are_multipliable(Matrix* a, Matrix* b);
bool is_quadratic(Matrix* a);
bool is_lower_triangular(Matrix* a);
bool is_upper_triangular(Matrix* a);
bool is_mirrored(Matrix* a);

int get_row_dimension(Matrix* a);
int get_columns_dimension(Matrix* a);

float* get_secondary_diagonal(Matrix* a);

Matrix* init(int row, int column, float* values);
Matrix* add_matrix(Matrix* a, Matrix* b);
Matrix* multiply(Matrix* a, float multiplier);
Matrix* multiply_2_matrices(Matrix* a, Matrix* b);
Matrix* matrix_power(Matrix* base, int exponent);



#endif // MATRIXLIBRARY_H_INCLUDED
