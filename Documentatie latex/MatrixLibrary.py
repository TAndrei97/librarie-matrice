
"""
 Print in a file a message followed by a matrix.
 message The message
 matrix The matrix
 file The output file
"""
def print_matrix(message, matrix, file):
    file.write(message+'\n')
    for i in range(len(matrix[0])):
        for j in range(len(matrix)):
            file.write(str(matrix[i][j]) + " ")
        file.write('\n')
    file.write('\n')

"""
 Print in a file an array
 array The array
 file The output file
"""
def print_array(array, file):
    for i in array:
        file.write(str(i) + ' ')
    file.write('\n')

"""
 Check if all elements above the main diagonal are zero.
 matrix The matrix
 return true if the matrix is lower triangular, otherwise false
"""
def is_lower_triangular(matrix):
    for i in range(len(matrix)-1):
        for j in range(i+1, len(matrix[0])):
            if matrix[i][j] != 0:
                return False
    return True

"""
 Check if all elements below the main diagonal are zero.
 matrix The matrix
 return true if the matrix is upper triangular, otherwise false
"""
def is_upper_triangular(matrix):
    for i in range(1, len(matrix)):
        for j in range(i):
            if matrix[i][j] != 0:
                return False
    return True

"""
 Check if symmetrically elements against the main diagonal or the second diagonal are equal.
 matrix The matrix
 return true if the matrix is mirrored, otherwise false
"""
def is_mirrored(matrix):
    is_mirrored_first = True
    is_mirrored_second = True
    for i in range(1, len(matrix)):
        for j in range(len(matrix[0])-1-i):
            if matrix[i][j] != matrix[j][i]:
                   is_mirrored_first = False
    for i in range(len(matrix)-1):
        for j in range(len(matrix[0])-1-i):
            if matrix[i][j] != matrix[len(matrix)-j-1][len(matrix[0])-i-1]:
                is_mirrored_second = False
    return is_mirrored_first or is_mirrored_second

"""
 Return the elements of the secondary diagonal
 matrix The matrix
"""
def get_secondary_diagonal(matrix):
    diagonal = []
    for i in range(len(matrix)):
        diagonal.append(matrix[i][len(matrix)-i-1])
    return diagonal

"""
 Compute the sum of the two matrix.
 matrix1 The first matrix
 matrix2 The second matrix
 return sum of two matrices
"""
def add_matrix(matrix1, matrix2):
    result = []
    for i in range(len(matrix1)):
        result.append([])
        for j in range(len(matrix1[0])):
            result[i].append(matrix1[i][j]+matrix2[i][j])
    return result

"""
 Compute the multiplication of the matrix by the multiplier.
 matrix The matrix
 multiplier The multiplier
 return the matrix multiplied by the multiplier
"""
def multiply(matrix, multiplier):
    result = []
    for i in range(len(matrix)):
        result.append([])
        for j in range(len(matrix[0])):
            result[i].append(matrix[i][j] * multiplier)
    return result

"""
 Compute the multiplication of two matrices.
 Multiply the elements of each row of the first matrix by the elements of each column in the second matrix.
 Add the products.
 matrix1 The first matrix
 matrix2 The second matrix
 return the multiplication of two matrices
"""

def multiply_2_matrices(matrix1, matrix2):
    result = []
    for i in range(len(matrix1)):
        result.append([])
        for j in range(len(matrix2[0])):
            aux = 0
            for k in range(len(matrix1[0])):
                aux += matrix1[i][k]*matrix2[k][j]
            result[i].append(aux)
    return result

"""
 Rise a matrix to a power
 base The matrix
 exponent The exponent
 return the matrix raised to the given power
"""
def matrix_power(base, exponent):
    result = base[:]
    exponent -= 1
    while exponent:
        result = multiply_2_matrices(result, base)
        exponent -= 1
    return result
    
                        
    
    
    
