/// \file MatrixAutoRun.c
/// \brief C library implementation to start to generate the matrices and to apply functions to them.
///

#include <stdio.h>
#include <stdlib.h>
#include "MatrixLibrary.h"
#include "MatrixTestGen.h"

    /// \fn void matrix_start_test(char* file_name)
    /// \brief Start the test of function
    ///
    /// \param file_name The name of the output file
void matrix_start_test(char* file_name) {

    ///
    /// Start to generate matrix and to apply functions form the MatrixLibrary to them.
    /// The matrix and the results are printed out in a file with given name.
    ///

    FILE* file;
    file = fopen(file_name, "w");
    int i;
    int random;
    int random_aux;

    fprintf(file, "Sum test:\n\n");
    for(i = 0; i < 20; i++) {
        random = rand()%1000;
        Matrix* a = matrix_generate(random);
        Matrix* b = matrix_generate(random);
        print_matrix(a, file, "Matrix 1:");
        print_matrix(b, file, "Matrix 2:");
        print_matrix(add_matrix(a, b), file, "Sum of matrix 1 and 2:");
        delete_matrix(a);
        delete_matrix(b);
    }


    fprintf(file, "Multiplication test:\n\n");
    for(i = 0; i < 20; i++) {
        random = rand()%1000;
        Matrix* a = matrix_generate(random);
        Matrix* b = matrix_generate(random);
        print_matrix(a, file, "Matrix 1:");
        print_matrix(b, file, "Matrix 2:");
        print_matrix(multiply_2_matrices(a, b), file, "Multiplication of matrix 1 and 2:");
        delete_matrix(a);
        delete_matrix(b);
    }

    fprintf(file, "Multiplication test:\n\n");
    for(i = 0; i < 20; i++) {
        random = rand()%1000;
        random_aux = rand()%20;
        Matrix* a = matrix_generate(rand()%1000);
        fprintf(file, "Multiplier = %d", random_aux);
        print_matrix(a, file, "Matrix:");
        print_matrix(multiply(a, random_aux), file, "Multiplication of matrix 1:");
        delete_matrix(a);

    }

    fprintf(file, "Power test:\n\n");
    for(i = 0; i < 20; i++) {
        random = rand()%1000;
        random_aux = rand()%20;
        Matrix* a = matrix_generate(random);
        fprintf(file, "Exponent = %d", random_aux);
        print_matrix(a, file, "Matrix:");
        print_matrix(multiply(a, random_aux), file, "Multiplication of matrix 1:");
        delete_matrix(a);

    }

    fprintf(file, "Get secundary diagonal:\n\n");
    for(i = 0; i < 20; i++) {
        random = rand()%1000;
        random_aux = rand()%20;
        Matrix* a = matrix_generate(random);
        print_matrix(multiply(a, random_aux), file, "Multiplication of matrix 1:");
        print_array(get_secondary_diagonal(a), 10, file);
        delete_matrix(a);

    }

     fprintf(file, "Mirrored:\n\n");
    for(i = 0; i < 20; i++) {
        random = rand()%1000;
        Matrix* a = matrix_generate_mirrored(random);
        Matrix* b = matrix_generate(random);
        print_matrix(a, file, "Matrix1:");
        print_matrix(b, file, "Matrix2:");
        fprintf(file, "Matrix1 is mirrored %d \n", is_mirrored(a));
        fprintf(file, "Matrix2 is mirrored %d \n", is_mirrored(b));
        delete_matrix(a);
        delete_matrix(b);
    }

     fprintf(file, "Triangular :\n\n");
    for(i = 0; i < 20; i++) {
        random = rand()%1000;
        Matrix* a = matrix_generate_triangular(random);
        Matrix* b = matrix_generate(random);
        print_matrix(a, file, "Matrix1:");
        print_matrix(b, file, "Matrix2:");
        fprintf(file, "Matrix1 is lower triangular %d \n", is_lower_triangular(a));
        fprintf(file, "Matrix1 is upper triangular %d \n", is_upper_triangular(a));
        fprintf(file, "Matrix2 is lower triangular %d \n", is_lower_triangular(b));
        fprintf(file, "Matrix2 is upper triangular %d \n", is_upper_triangular(b));
        delete_matrix(a);
        delete_matrix(b);
    }
}
