from random import randint

"""
 Generate a random matrix
 dimension The dimension of the matrix
 return a random matrix with dimension x dimension elements
"""
def matrix_generate(dimension):
    matrix = []
    for i in range(dimension):
        matrix.append([])
        for j in range(dimension):
            matrix[i].append(randint(0,9))
    return matrix

"""
 Generate a random triangular matrix
 dimension The dimension of the matrix
 return a random triangular matrix with dimension x dimension elements
"""
def matrix_generate_triangular(dimension):
    triangular = randint(0,1)
    matrix = []
    if triangular == 0:
        for i in range(dimension):
            matrix.append([])
            for j in range(i):
                matrix[i].append(0)
            for j in range(i, dimension):
                matrix[i].append(randint(0,9))
    else:
        for i in range(dimension):
            matrix.append([])
            for j in range(i+1):
                matrix[i].append(randint(0,9))
            for j in range(i+1, dimension):
                matrix[i].append(0)
    return matrix

"""
 Generate a random mirrored matrix
 dimension The dimension of the matrix
 return a random mirrored matrix with dimension x dimension elements
"""
def matrix_generate_mirrored(dimension):
    mirrored = randint(0,1)
    matrix = []
    for i in range(dimension):
        matrix.append([])
        for j in range(dimension):
            matrix[i].append(0)
    if mirrored == 0:
        for i in range(1, dimension):
            for j in range(i):
                matrix[i][j] = matrix[j][i] = randint(0,9)
            matrix[i][i] = randint(0,9)
    else:
        for i in range(dimension):
            for j in  range(dimension-i-1):
                matrix[i][j] = matrix[dimension-j-1][dimension-i-1] = randint(0,9)
            matrix[i][dimension-1-i] = randint(0,9)
    return matrix
            
                
