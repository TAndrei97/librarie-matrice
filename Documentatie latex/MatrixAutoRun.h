/// \file MatrixAutoRun.c
/// \brief C library implementation to start to generate the matrices and to apply functions to them.
///

#ifndef MATRIXAUTORUN_H_INCLUDED
#define MATRIXAUTORUN_H_INCLUDED
#include "MatrixLibrary.h"
#include "MatrixTestGen.h"

void matrix_start_test(char* file_name);


#endif // MATRIXAUTORUN_H_INCLUDED
