/// \file MatrixLibrary.c
/// \brief C library implementation for matrix functions.
///


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include "MatrixLibrary.h"

    /// \brief Structure to store a matrix
    ///
typedef struct Matrix {


    int columns; ///< Store the number of columns of the matrix.
    int rows; ///< Store the number of rows of the matrix.
    float mat[]; ///< Store the value of the elements of the matrix.
}Matrix;

    /// \fn void print_matrix(Matrix* mat, FILE* file, char* mess)
    /// \brief Print a message and a matrix in a file
    ///
    /// \param mat The matrix
    /// \param file The output file
    /// \param mess The message
void print_matrix(Matrix* mat, FILE* file, char* mess) {

    ///
    /// Print in a file a message followed by a matrix.
    ///



    int i;
    int j;

    fprintf(file, "%s\n", mess);
    for(i = 0; i < mat->rows; i++) {
        for (j = 0; j < mat->columns; j++) {
            fprintf(file, "%.0f ", mat->mat[i * mat->columns + j]);
        }
        fprintf(file, "\n");
    }
    fprintf(file, "\n");
}

    /// \fn void print_array(float* a, int length, FILE* file)
    /// \brief Print an array in a file
    ///
    /// \param a The array
    /// \param length The length of the array
    /// \param file The output file
void print_array(float* a, int length, FILE* file) {

    ///
    /// Print in a file an array of a given length.
    ///

    int i;
    fprintf(file, "\n");
    for(i = 0; i < length; i++) {
        fprintf(file, "%f ", a[i]);
    }
    fprintf(file, "\n");
}

    /// \fn void delete_matrix(Matrix* a)
    /// \brief Release the memory of a matrix
    ///
    /// \param a The matrix
void delete_matrix(Matrix* a) {

    ///
    /// Release the memory used by a matrix.
    ///

    free(a);
}


    /// \fn bool are_summable(Matrix* a, Matrix* b)
    /// \brief Check if two matrix are summable
    ///
    /// \param a The first matrix
    /// \param b The second matrix
    /// \return True if the matrix are summable, otherwise false
bool are_summable(Matrix* a, Matrix* b) {

    ///
    /// Check if the first matrix and the second one have the same size and return the result.
    ///

    return a->columns == b->columns && a->rows == b->rows;

}

    /// \fn bool are_multipliable(Matrix* a, Matrix* b)
    /// \brief Check if two matrix are multipliable
    ///
    /// \param a The first matrix
    /// \param b The second matrix
    /// \return True if the matrix are multipliable, otherwise false
bool are_multipliable(Matrix* a, Matrix* b) {


    ///
    /// Check if the number of columns of the first matrix and the number of rows of the second matrix are equal and return the result.
    ///

    return a->columns == b->rows;

}

    /// \fn bool is_quadratic(Matrix* a)
    /// \brief Check if a matrix is quadratic
    ///
    /// \param a The matrix
    /// \return True if the matrix is quadratic, otherwise false
bool is_quadratic(Matrix* a) {

    ///
    /// Check if the number of rows and the number of columns of a matrix are equal and return the result.
    ///

    return a->columns == a->rows;

}

    /// \fn bool is_lower_triangular(Matrix* a)
    /// \brief Check if a matrix is lower triangular
    ///
    /// \param a The matrix
    /// \return True if the matrix is lower triangular, otherwise false
bool is_lower_triangular(Matrix* a) {

    ///
    /// Check if all elements above the main diagonal are zero.
    ///


    int i;
    int j;

    assert(is_quadratic(a));

    for(i = 0 ; i < a->rows - 1; i++) {
        for(j = i + 1; j < a->columns; j++) {
            if(a->mat[i * a->rows + j] != 0)
                return false;
        }
    }
    return true;
}

    /// \fn bool is_upper_triangular(Matrix* a)
    /// \brief Check if a matrix is upper triangular
    ///
    /// \param a The matrix
    /// \return True if the matrix is upper triangular, otherwise false
bool is_upper_triangular(Matrix* a) {

    ///
    /// Check if all elements below the main diagonal are zero.
    ///

    int i;
    int j;

    assert(is_quadratic(a));

    for(i = 1; i < a->rows; i++) {
        for(j = 0; j < i; j++) {
            if(a->mat[i * a->rows + j] != 0)
                return false;
        }
    }
    return true;
}

    /// \fn bool is_mirrored(Matrix* a)
    /// \brief Check if a matrix is mirrored
    ///
    /// \param a The matrix
    /// \return True if the matrix is mirrored, otherwise false
bool is_mirrored(Matrix* a) {

    ///
    /// Check if symmetrically elements against the main diagonal or the second diagonal are equal.
    ///

    int i;
    int j;
    bool is_mirrored_first = true;
    bool is_mirrored_second = true;

    assert(is_quadratic(a));

    for(i = 1; i < a->rows; i++) {
        for(j = 0; j < i; j++) {
            if(a->mat[i * a->rows + j] != a->mat[j * a->rows + i])
                is_mirrored_first = false;
        }
    }

    for(i = 0; i < a->rows - 1; i++) {
        for(j = 0; j < a->columns - 1 - i; j++) {
            if(a->mat[i * a->rows + j] != a->mat[(a->rows - j - 1) * a->rows + (a->columns - i - 1)])
                is_mirrored_second = false;
        }
    }

    return is_mirrored_first || is_mirrored_second;

}

    /// \fn int get_row_dimension(Matrix* a)
    /// \brief Return the number of rows of a matrix
    ///
    /// \param a The matrix
    /// \return Number of rows
int get_row_dimension(Matrix* a) {

    ///
    /// Return the number of rows of a matrix.
    ///

    return a->rows;
}

    /// \fn int get_columns_dimension(Matrix* a)
    /// \brief Return the number of columns of a matrix
    ///
    /// \param a The matrix
    /// \return Number of columns
int get_columns_dimension(Matrix* a) {

    ///
    /// Return the number of columns of a matrix.
    ///

    return a->columns;
}

    /// \fn float* get_secondary_diagonal(Matrix* a)
    /// \brief Return the elements of the secondary diagonal
    ///
    /// \param a The matrix
    /// \return The elements of the secondary diagonal
float* get_secondary_diagonal(Matrix* a) {

    ///
    /// Return a pointer to the elements of the secondary diagonal.
    ///

    float* values = (float*)malloc(a->rows * sizeof(float));
    int  i;

    assert(is_quadratic(a));

    for(i = 0; i < a->rows; i++) {
        values[i] = a->mat[i * a->rows + a->rows - i - 1];
    }

    return values;
}

    /// \fn Matrix* init(int row, int column, float* values)
    /// \brief Create a structure of type matrix
    ///
    /// \param row The number of rows
    /// \param column The number of columns
    /// \param values The values of the elements
    /// \return A pointer to a structure of type matrix
Matrix* init(int row, int column, float* values) {

    ///
    /// Create a structure of type matrix with the given characteristics.
    /// Return a pointer to that structure.
    ///

    Matrix* mat = (Matrix*)malloc(sizeof(Matrix) + row * column * sizeof(float));
    int i;
    int j;

    mat->rows = row;
    mat->columns = column;

    for(i = 0; i < row; i++) {
        for(j = 0; j < column; j++) {
            mat->mat[i*column+j] = values[i*column+j];
        }
    }

    free(values);

    return mat;

}

    /// \fn Matrix* add_matrix(Matrix* a, Matrix* b)
    /// \brief Add two matrices
    ///
    /// \param a The first matrix
    /// \param b The second matrix
    /// \return Sum of two matrices
Matrix* add_matrix(Matrix* a, Matrix* b) {

    ///
    /// Compute the sum of the two matrix.
    /// Create another structure of type matrix with the values of sum of the matrices.
    /// Return a pointer to that structure.
    ///

    float* sum_values;
    int i;
    int j;

    assert(are_summable(a, b));

    sum_values = (float*)malloc(a->columns * a->rows * sizeof(float));
    for(i = 0; i < a->rows; i++) {
        for(j = 0; j < a->columns; j++) {
            sum_values[i*a->columns+j] = a->mat[i*a->columns+j] + b->mat[i*a->columns+j];
        }
    }

    return init(a->rows, a->columns, sum_values);
}

    /// \fn Matrix* multiply(Matrix* a, float multiplier)
    /// \brief Multiply a matrix by a number
    ///
    /// \param a The matrix
    /// \param multiplier The multiplier
    /// \return The matrix multiplied by the multiplier
Matrix* multiply(Matrix* a, float multiplier) {

    ///
    /// Compute the multiplication of the matrix by the multiplier.
    /// Create another structure of type matrix with the value of the matrix multiplied by the multiplier.
    /// Return a pointer to that structure.
    ///

    int i;
    int j;
    float* multiplied_values = (float*)malloc(a->rows * a->columns * sizeof(float));

    for(i = 0; i < a->rows; i++) {
        for(j = 0; j < a->columns; j++) {
            multiplied_values[i*a->columns+j] = a->mat[i*a->columns+j] * multiplier;
        }
    }

    return init(a->rows, a->columns, multiplied_values);
}


    /// \fn Matrix* multiply_2_matrices(Matrix* a, Matrix* b)
    /// \brief Multiply two matrices
    ///
    /// \param a The first matrix
    /// \param b The second matrix
    /// \return The multiplication of two matrices
Matrix* multiply_2_matrices(Matrix* a, Matrix* b) {

    ///
    /// Compute the multiplication of two matrices.
    /// Multiply the elements of each row of the first matrix by the elements of each column in the second matrix.
    /// Add the products.
    /// Create another structure of type matrix with the value of the matrices multiplication.
    /// Return a pointer to that structure.
    ///

    int i;
    int j;
    int k;
    int it = 0;
    float* multiplied_values = (float*)calloc(a->rows * b->columns, sizeof(float));

    assert(are_multipliable(a, b));

    for(i = 0; i < a->rows; i++) {
        for(j = 0; j < b->columns; j++) {
            for(k = 0; k < a->columns; k++) {
                multiplied_values[it] += a->mat[i * a->rows + k] * b->mat[k * b->columns + j];
            }
            it++;
        }
    }

    return init(a->rows, a->columns, multiplied_values);
}

    /// \fn Matrix* matrix_power(Matrix* base, int exponent)
    /// \brief Rise a matrix to a power
    ///
    /// \param base The matrix
    /// \param exponent The exponent
    /// \return The matrix raised to the given power
Matrix* matrix_power(Matrix* base, int exponent) {

    ///
    /// Multiply the matrix exponent-th times.
    /// Create another structure of type matrix with the value of the matrix raised to the given power.
    /// Return a pointer to that structure.
    ///

    assert(is_quadratic(base));

    Matrix* aux = init(base->rows, base->rows, base->mat);

    exponent--;

    while(exponent > 0) {
        aux = multiply_2_matrices(aux, base);
        exponent --;
    }

    return aux;

}









