/// \file MatrixAutoRun.c
/// \brief C library implementation to start to generate the matrices and to apply functions to them.
///

#include <stdio.h>
#include <stdlib.h>
#include "MatrixLibrary.h"
#include "MatrixTestGen.h"

    /// \fn void matrix_start_test(char* file_name)
    /// \brief Start the test of function
    ///
    /// \param file_name The name of the output file
void matrix_start_test(char* file_name) {

    ///
    /// Start to generate matrix and to apply functions form the MatrixLibrary to them.
    /// The matrix and the results are printed out in a file with given name.
    ///

    FILE* file;
    file = fopen(file_name, "w");
    int i;
    int random;
    int dimension;
    int test_number = 5;
    int max_dimension = 1000;

    fprintf(file, "Sum test:\n\n");
    for(i = 0; i < test_number; i++) {
        dimension = rand()%max_dimension;
        Matrix* a = matrix_generate(dimension);
        Matrix* b = matrix_generate(dimension);
        print_matrix(a, file, "Matrix 1:");
        print_matrix(b, file, "Matrix 2:");
        print_matrix(add_matrix(a, b), file, "Sum of matrix 1 and 2:");
        delete_matrix(a);
        delete_matrix(b);
    }


    fprintf(file, "Multiplication test:\n\n");
    for(i = 0; i < test_number; i++) {
        dimension = rand()%max_dimension;
        Matrix* a = matrix_generate(dimension);
        Matrix* b = matrix_generate(dimension);
        print_matrix(a, file, "Matrix 1:");
        print_matrix(b, file, "Matrix 2:");
        print_matrix(multiply_2_matrices(a, b), file, "Multiplication of matrix 1 and 2:");
        delete_matrix(a);
        delete_matrix(b);
    }

    fprintf(file, "Multiplication test:\n\n");
    for(i = 0; i < test_number; i++) {
        dimension = rand()%max_dimension;
        random = rand()%20;
        Matrix* a = matrix_generate(dimension);
        fprintf(file, "Multiplier = %d\n", random);
        print_matrix(a, file, "Matrix:");
        print_matrix(multiply(a, random), file, "The matrix multiplied by the multiplier:");
        delete_matrix(a);

    }

    fprintf(file, "Power test:\n\n");
    for(i = 0; i < test_number; i++) {
        dimension = rand()%max_dimension;
        random = rand()%10;
        Matrix* a = matrix_generate(dimension);
        fprintf(file, "\nExponent = %d\n", random);
        print_matrix(a, file, "Matrix:");
        print_matrix(multiply(a, random), file, "The matrix raised to the exponent:");
        delete_matrix(a);

    }

    fprintf(file, "Get secondary diagonal:\n\n");
    for(i = 0; i < test_number; i++) {
        dimension = rand()%max_dimension;
        Matrix* a = matrix_generate(dimension);
        fprintf(file, "The elements of secondary diagonal are:\n");
        print_array(get_secondary_diagonal(a), dimension, file);
        delete_matrix(a);

    }

     fprintf(file, "Mirrored:\n\n");
    for(i = 0; i < test_number; i++) {
        dimension = rand()%max_dimension;
        Matrix* a = matrix_generate_mirrored(dimension);
        Matrix* b = matrix_generate(dimension);
        print_matrix(a, file, "Matrix:");
        fprintf(file, "The matrix is mirrored = %d \n", is_mirrored(a));
        print_matrix(b, file, "Matrix2:");
        fprintf(file, "The matrix is mirrored = %d \n", is_mirrored(b));
        delete_matrix(a);
        delete_matrix(b);
    }

     fprintf(file, "Triangular :\n\n");
    for(i = 0; i < test_number; i++) {
        dimension = rand()%max_dimension;
        Matrix* a = matrix_generate_triangular(dimension);
        Matrix* b = matrix_generate(dimension);
        print_matrix(a, file, "Matrix:");
        fprintf(file, "The matrix is lower triangular = %d \n", is_lower_triangular(a));
        fprintf(file, "The matrix is upper triangular = %d \n", is_upper_triangular(a));
        print_matrix(b, file, "Matrix:");
        fprintf(file, "The matrix is lower triangular = %d \n", is_lower_triangular(b));
        fprintf(file, "The matrix is upper triangular = %d \n", is_upper_triangular(b));
        delete_matrix(a);
        delete_matrix(b);
    }
}
